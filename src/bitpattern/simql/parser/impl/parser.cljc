(ns bitpattern.simql.parser.impl.parser
  "Parser for the SimQL grammar."
  (:require [clojure.string :as cstr]
            [instaparse.core :as insta]))

(def ^:private grammar
  "expr := expr-conc?

   expr-complement := <bang> expr-conc
   <expr-conc> := expr-statement | compound-statement | <whitespace>
   <expr-conc-conj> := expr-statement | conjunction
   <expr-statement> := expr-complement | statement | wrapped-expr-conc
   <wrapped-expr-conc> := open-paren expr-conc? close-paren

   statement := !expr-complement term
   conjunction := expr-statement (<and> expr-statement)+
   disjunction := expr-conc-conj (<or-ws> expr-conc-conj)+
   <compound-statement> := conjunction | disjunction

   <term> := field-search | !field-search string-search

   field-search := field operator field-value
   string-search := wildcard? text-string wildcard?
   field-value := !eq (field-string | wildcard)
   field-string := wildcard-string | empty-string | quoted-string | !wildcard simple-field-value

   field := unquoted-string
   operator := #'[!:=\\<\\>\\$]+'
   wildcard := <'*'>
   dblquote := <'\"'>

   <wildcard-string> := dblquote wildcard dblquote
   <empty-string> := dblquote nothing dblquote
   <str-open> = dblquote wildcard?
   <str-close> = wildcard? dblquote
   <or-ws> := whitespace or whitespace

   <quoted-string> := str-open quoteless-string str-close
   <text-string> := str-open quoteless-string? str-close | !wildcard unquoted-string

   <simple-field-value> := #'[^\"\\(\\s\\)!:=\\<\\>\\$]*'
   <unquoted-string> := #'[a-zA-Z0-9.,_-]+'

   <open-paren> := <'('>
   <close-paren> := <')'>
   <bang> := '!'
   <eq> := '='
   <and> := <whitespace> !or
   <or> := 'OR'
   <whitespace> := #'\\s+'
   <nothing> := #''

   <quoteless-string> = #'[^\"*]+'")

(def ^:private parser (insta/parser grammar :string-ci true))

(defn parses
  "Produce all parse trees for the given query."
  [query]
  (insta/parses parser (cstr/trim query)))

(defn- throw-query-parse-exception
  [query]
  (throw (ex-info "Exception while parsing query" {:query query})))

(defn parse
  "Parse the query."
  [query]
  (try
    (let [tree (insta/parse parser (cstr/trim query))]
      (if (map? tree)
        (throw (ex-info "Unable to parse query" {:failure tree}))
        tree))
    (catch #?(:clj Exception :cljs js/Error) e
      (throw-query-parse-exception query))))
