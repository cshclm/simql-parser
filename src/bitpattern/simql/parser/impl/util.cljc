(ns bitpattern.simql.parser.impl.util
  "Generic utility functions.")

(defn assoc-if
  "Assoc only if `v` is truthy."
  [m k v]
  (if v
    (assoc m k v)
    m))

(defn tree-path
  "Return a path satisfying `pred` in the given tree."
  ([pred tree]
   (tree-path pred tree {:idx 0 :path []}))
  ([pred [h & rem] acc]
   (if (sequential? h)
     (if (pred h)
       (recur pred h
              (-> acc
                  (update :path conj (:idx acc))
                  (assoc :idx 0)))
       (recur pred rem (update acc :idx inc)))
     (:path acc))))

(defn prune-tree
  "Return a new tree with only the branches returned by `prune-fn`."
  [prune-fn tree]
  (if (sequential? tree)
    (with-meta (mapv #(prune-tree prune-fn %) (prune-fn tree))
      (meta tree))
    tree))

(defn- context-paths
  [path]
  (let [n (dec (count path))]
    (->> path
         (take n)
         (reductions conj [])
         rest
         reverse)))

(defn path-context
  "Search `path` in reverse, returning the deepest context from the tree."
  [extractor tree path]
  (reduce
   (fn [acc p]
     (let [node (get-in tree p)]
       (if-let [ctx (extractor (get-in tree p))]
         (reduced ctx))))
   nil (context-paths path)))
