(ns bitpattern.simql.parser.impl.transformer
  "Validate and transform a parse tree."
  (:require
   [bitpattern.simql.parser.datatypes :as datatypes]
   [instaparse.core :as insta]))

(defn- validate-operation
  [datatype field-str op]
  (when-not (datatypes/valid-operation? datatype op)
    (throw (ex-info
            "Operation not valid for datatype"
            {:simql/result :simql/invalid-operation
             :simql/operation op
             :simql/datatype datatype
             :simql/field-name field-str}))))

(defn- get-datatype
  [fields field-str]
  (if-let [datatype (:datatype (get fields field-str))]
    datatype
    (throw (ex-info
            "Datatype for field could not be found"
            {:simql/result :simql/invalid-field
             :simql/field-name field-str}))))

(defn- lookup-datatype
  [fields field-str op]
  (let [datatype (get-datatype fields field-str)]
    (validate-operation datatype field-str op)
    datatype))

(defn parse-value
  [dt vs]
  (if (= vs [:wildcard])
    [dt vs]
    (datatypes/read-value dt vs)))

(defn- transform-field-values
  [dt [vk values :as value-data]]
  (with-meta [vk (with-meta (parse-value dt values)
                   (meta values))]
    (meta value-data)))

(defn- transform-field-search
  [fields
   [_ field :as field-data]
   [_ op :as op-data]
   vs]
  [:field-search field-data op-data
   (-> fields
       (lookup-datatype field op)
       (transform-field-values vs))])

(defn- transform-string-search
  [& vs]
  [:string-search (into [:string] vs)])

(defn- transforms [fields]
  {:field-search (partial transform-field-search fields)
   :string-search transform-string-search})

(defn transform [fields parse-tree]
  "Transform the parse-tree using the provided field definitions."
  (insta/transform (transforms fields) parse-tree))
