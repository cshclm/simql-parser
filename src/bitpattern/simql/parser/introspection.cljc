(ns bitpattern.simql.parser.introspection
  "Introspect the metadata on a parse tree."
  (:require [bitpattern.simql.parser.impl.parser :as parser]
            [bitpattern.simql.parser.impl.util :as util]
            [clojure.string :as cstr]
            [instaparse.core :as insta]))

(defn- span-at-point?
  [point [start end]]
  (and (> point start)
       (<= point end)))

(defn- branch-at-point?
  [point parse-tree]
  (->> parse-tree
       insta/span
       (span-at-point? point)))

(defn- prune-parse-tree
  [parse-tree]
  (->> parse-tree
       rest
       (remove #(= [:wildcard] %))
       vec))

(defn- parse-tree-path
  [point parse-tree]
  (util/tree-path #(branch-at-point? point %) parse-tree))

(defn- parse-tree-path-satisfying-point
  [point parse-tree]
  (->> parse-tree
       (util/prune-tree prune-parse-tree)
       (parse-tree-path point)
       (mapv inc)))

(defn- char->string
  [c]
  (cond
    (= c :dblquote) ""
    (= c :wildcard) "*"
    :default c))

(defn- nodes->string
  [nodes]
  (->> nodes
       (mapcat seq)
       (map char->string)
       (apply str)))

(defn- thing-at-path
  [parse-tree path]
  (let [node (get-in parse-tree path)
        span (insta/span node)]
    {:span span
     :type (first node)
     :value (vec (next node))
     :string-value (nodes->string (next node))}))

(defn- path-field-context
  [parse-tree path]
  (letfn [(extract-field-context [node]
            (when (= (first node) :field-search)
              (apply hash-map (second node))))]
    (util/path-context extract-field-context parse-tree path)))

(defn- string-to-point
  "Return the thing, up to point."
  [point {:keys [span value]}]
  (let [[start end] span
        len (- point start)
        elts (mapcat seq value)]
    (->> elts
         (map char->string)
         (take len)
         (apply str))))

(defn- describe-span-at-point
  [point parse-tree]
  (let [path (parse-tree-path-satisfying-point point parse-tree)
        thing (thing-at-path parse-tree path)]
    (-> thing
        (util/assoc-if
         :string-to-point (string-to-point point thing))
        (util/assoc-if
         :context (and
                   (not= (:type thing) :field)
                   (path-field-context parse-tree path))))))

(defn describe-point
  "Return the context of the token at the given point of the query."
  [query point]
  (let [trimmed-query (cstr/triml query)
        tree (parser/parse trimmed-query)
        np (- point (- (count query) (count trimmed-query)))]
    (describe-span-at-point np tree)))
