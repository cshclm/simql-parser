(ns bitpattern.simql.parser.datatypes
  "Datatypes for SimQL."
  (:require [clojure.edn :as edn]))

(defn- match-value
  [f vs]
  (and (= (count vs) 2)
       (f (second vs))
       (second vs)))

(defmulti read-value
  "Read the value `v` for datatype `k`."
  (fn [k v] k))

(defmethod read-value :string
  [_ [k & vs :as field-values]]
  (into [:string] vs))

(defmethod read-value :integer
  [_ vs]
  (if-let [v (match-value #(re-matches #"-?[0-9]+" %) vs)]
    [:integer (edn/read-string v)]
    (throw (ex-info (str "An integer was required but was not found")
                    {:field-value vs}))))

(defmethod read-value :number
  [_ vs]
  (if-let [v (match-value #(re-matches #"-?[0-9]+(\.[0-9]+)?" %) vs)]
    [:number (edn/read-string v)]
    (throw (ex-info (str "A number was required but was not found")
                    {:field-value vs}))))

(defmethod read-value :boolean
  [_ vs]
  (if-let [v (match-value #(re-matches #"(?i)true|false" %) vs)]
    [:boolean (edn/read-string v)]
    (throw (ex-info (str "A boolean was required but was not found")
                    {:field-value vs}))))

(defmulti valid-operation?
  "Returns `true` if the operation `op` is valid for the datatype `dt`."
  (fn [dt op] dt))

(defmethod valid-operation? :boolean
  [_ op]
  (boolean (#{"=" ":" "=="} op)))

(defmethod valid-operation? :default
  [_ _] true)
