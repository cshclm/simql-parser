(ns bitpattern.simql.parser.core
  "Parser for SimQL, the simple query language."
  (:require
   [bitpattern.simql.parser.impl.parser :as parser]
   [bitpattern.simql.parser.impl.transformer :as transformer]
   [clojure.string :as cstr]))

(defn create-parser
  "Build a SimQL query parser with the given configuration."
  [{:keys [fields]}]
  (fn [query]
    (->> query
         (parser/parse)
         (transformer/transform fields))))
