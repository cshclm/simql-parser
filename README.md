# SimQL, a simple query language

[![pipeline status](https://gitlab.com/cshclm/simql-parser/badges/master/pipeline.svg)](https://gitlab.com/cshclm/simql-parser/commits/master)
[![coverage report](https://gitlab.com/cshclm/simql-parser/badges/master/coverage.svg)](https://gitlab.com/cshclm/simql-parser/commits/master)

SimQL is a simple, light-weight query language for in-product search.

Part of most any application is providing a way for a user to search to locate
the records they need.

However, in-product search tends to be either extremely limited, or it exposes
the underlying technology stack (e.g., SQL, Lucene, etc.). Neither of these
situations make for a good user experience.

This repository provides a Parser for SimQL.

You may also be interested in [SimQL
Typeahead](https://gitlab.com/cshclm/simql-typeahead/), a React/Reagent
component for typeahead which is SimQL-aware.

## Installation

`deps.edn`:
```clj
{:deps
 {camelot/simql-parser {:git/url "https://gitlab.com/cshclm/simql-parser"
                        :sha "<LATEST COMMIT HASH>"}
 ...}}
```

## Usage

```clj
(ns your.namespace
  (:require [bitpattern.simql.parser.core :as simql-parser]))

(def parse
  (simql-parser/create-parser
    {:fields {"quantity" {:datatype :number}
              "animal" {:datatype :string}}}))

(parse "animal:cat quantity>=10")
```

Produces:

```clj
[:expr
  [:conjunction
   [:statement
    [:field-search
     [:field "animal"]
     [:operator ":"]
     [:field-value [:string "cat"]]]]
   [:statement
    [:field-search
     [:field "quantity"]
     [:operator ">="]
     [:field-value [:number 10]]]]]]
```

### Datatypes

As shown in the usage above, fields have datatypes.

The following datatypes are built-in: `:string`, `:integer`, `:number`,
`:boolean`. Datatypes are open for extension though, so it is also possible to
define your own datatypes.  For example, to introduce a `:data` datatype:

```clj
(ns your.namespace
  (:require [bitpattern.simql.parser.datatypes :as simql-dt]))))

(defmethod simql-dt/read-value :date
  [k vs]
  (let [v (first (filter string? vs))]
    [k (to-date v)]))
```

### Operations

Not all operations make sense on all datatypes. For example, this would
probably not be a sensible expression:

`are-we-there-yet <= false`

If `:boolean` were your custom datatype, you could constrain the operations as
follows:

```clj
(ns your.namespace
  (:require [bitpattern.simql.parser.datatypes :as simql-dt]))))

(defmethod valid-operation? :boolean
  [_ op]
  (boolean (#{"=" ":" "=="} op)))
```

## License

Copyright © 2020 Chris Mann

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
