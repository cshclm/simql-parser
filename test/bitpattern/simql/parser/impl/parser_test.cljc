(ns bitpattern.simql.parser.impl.parser-test
  (:require [bitpattern.simql.parser.impl.parser :as sut]
            #?(:clj [clojure.test :refer [deftest testing is are]]
               :cljs [cljs.test :refer [deftest testing is are] :include-macros true])))

(defn dblquote
  [k & args]
  (conj (apply vector k [:dblquote] args) [:dblquote]))

(defmacro is-parse=
  [value query]
  `(do
     (is (= [~value] (sut/parses ~query)))
     (is (= ~value (sut/parse ~query)))))

(deftest test-parses
  (testing "parses"
    (testing "should give the expected result for invalid queries"
      (testing "when missing a closing quote delimiter"
        (is (thrown? #?(:clj Exception :cljs js/Error)
                     (sut/parse "\"field"))))

      (testing "when missing an opening quote delimiter"
        (is (thrown? #?(:clj Exception :cljs js/Error)
                     (sut/parse "field\""))))

      (testing "when missing a closing paren"
        (is (thrown? #?(:clj Exception :cljs js/Error)
                     (sut/parse "(field"))))

      (testing "when missing an opening paren"
        (is (thrown? #?(:clj Exception :cljs js/Error)
                     (sut/parse "field)"))))

      (testing "when parens do not balance"
        (is (thrown? #?(:clj Exception :cljs js/Error)
                     (sut/parse "((field)))"))))

      (testing "when parens are used in an unquoted field search"
        (is (thrown? #?(:clj Exception :cljs js/Error)
                     (sut/parse "field:someth(ing"))))

      (testing "when colon eq operator is used in an unquoted field search"
        (is (thrown? #?(:clj Exception :cljs js/Error)
                     (sut/parse "field:someth:ing"))))

      (testing "when lt operator is used in an unquoted field search"
        (is (thrown? #?(:clj Exception :cljs js/Error)
                     (sut/parse "field:someth<ing"))))

      (testing "when gt operator is used in an unquoted field search"
        (is (thrown? #?(:clj Exception :cljs js/Error)
                     (sut/parse "field:someth>ing"))))

      (testing "when '=' eq operator is used in an unquoted field search"
        (is (thrown? #?(:clj Exception :cljs js/Error)
                     (sut/parse "field:someth=ing")))))

    (testing "should produce empty parse trees"
      (testing "when given a blank string"
        (is-parse= [:expr] ""))

      (testing "when given only whitespace"
        (is-parse= [:expr] " "))

      (testing "when given empty parens"
        (is-parse= [:expr] "()"))

      (testing "when given nested empty parens"
        (is-parse= [:expr] "((()))"))

      (testing "when given empty parens with a space between"
        (is-parse= [:expr] "( )")))

    (testing "should produce simple text search parse trees"
      (testing "when given whitespace before a word"
        (is-parse= [:expr [:statement
                           [:string-search "aardvark"]]]
                   " aardvark"))

      (testing "when given whitespace after a word"
        (is-parse= [:expr [:statement
                           [:string-search "aardvark"]]]
                   "aardvark "))

      (testing "when given a single word"
        (is-parse= [:expr [:statement
                           [:string-search "aardvark"]]]
                   "aardvark"))

      (testing "when given a single word wrapped in parens"
        (is-parse= [:expr [:statement
                           [:string-search "aardvark"]]]
                   "(aardvark)"))

      (testing "when given a wildcard after the term"
        (is-parse= [:expr [:statement
                           [:string-search "aa" [:wildcard]]]]
                   "aa*"))

      (testing "when given a wildcard before the term"
        (is-parse= [:expr [:statement
                           [:string-search [:wildcard] "aa"]]]
                   "*aa"))

      (testing "when given a quoted phrase"
        (is-parse= [:expr [:statement
                           (dblquote :string-search "It's all down-hill from here")]]
                   "\"It's all down-hill from here\"")))

    (testing "should produce parse trees with disjunctions"
      (testing "when given a disjunction of two words"
        (is-parse= [:expr [:disjunction
                           [:statement [:string-search "Jane"]]
                           [:statement [:string-search "John"]]]]
                   "Jane OR John"))

      (testing "when given an 'OR' keyword in various cases"
        (are [or] (is-parse= [:expr [:disjunction
                                     [:statement [:string-search "Jane"]]
                                     [:statement [:string-search "John"]]]]
                             (str "Jane " or " John"))
          "or"
          "Or"
          "oR"))

      (testing "when given a disjunction of field values"
        (is-parse= [:expr [:disjunction
                           [:statement
                            [:field-search
                             [:field "firstname"]
                             [:operator ":"]
                             [:field-value [:field-string "jane"]]]]
                           [:statement
                            [:field-search
                             [:field "firstname"]
                             [:operator ":"]
                             [:field-value [:field-string "john"]]]]]]
                   "firstname:jane OR firstname:john")))

    (testing "should produce parse trees with conjunctions"
      (testing "when given a conjunction of two words"
        (is-parse= [:expr [:conjunction
                           [:statement [:string-search "chalk"]]
                           [:statement [:string-search "cheese"]]]]
                   "chalk cheese"))

      (testing "when given a conjunction of field values"
        (is-parse= [:expr [:conjunction
                      [:statement
                       [:field-search
                        [:field "firstname"]
                        [:operator ":"]
                        [:field-value [:field-string "jane"]]]]
                      [:statement
                       [:field-search
                        [:field "surname"]
                        [:operator ":"]
                        [:field-value [:field-string "smith"]]]]]]
                   "firstname:jane surname:smith")))

    (testing "should produce parse trees with field searches"
      (testing "when given a field search using the ':' :eq operator"
        (is-parse= [:expr [:statement
                     [:field-search
                      [:field "power"]
                      [:operator ":"]
                      [:field-value [:field-string "max"]]]]]
                   "power:max"))

      (testing "when numbers are given as the value"
        (is-parse= [:expr [:statement
                      [:field-search
                       [:field "stars"]
                       [:operator ":"]
                       [:field-value [:field-string "5"]]]]]
                   "stars:5"))

      (testing "when given a field search using the '=' operator"
        (is-parse= [:expr [:statement
                     [:field-search
                      [:field "power"]
                      [:operator "="]
                      [:field-value [:field-string "max"]]]]]
                   "power=max"))

      (testing "when given a field search using the '==' operator"
        (is-parse= [:expr [:statement
                      [:field-search
                       [:field "power"]
                       [:operator "=="]
                       [:field-value [:field-string "max"]]]]]
                   "power==max"))

      (testing "when given a field search using the '<=' operator"
        (is-parse= [:expr [:statement
                     [:field-search
                      [:field "power"]
                      [:operator "<="]
                      [:field-value [:field-string "max"]]]]]
                   "power<=max"))

      (testing "when given a field search using the '>=' operator"
        (is-parse= [:expr [:statement
                      [:field-search
                       [:field "power"]
                       [:operator ">="]
                       [:field-value [:field-string "max"]]]]]
                   "power>=max"))

      (testing "when given a field search using the '>' operator"
        (is-parse= [:expr [:statement
                     [:field-search
                      [:field "power"]
                      [:operator ">"]
                      [:field-value [:field-string "max"]]]]]
                   "power>max"))

      (testing "when given a field search using the '<' operator"
        (is-parse= [:expr [:statement
                      [:field-search
                       [:field "power"]
                       [:operator "<"]
                       [:field-value [:field-string "max"]]]]]
                   "power<max"))

      (testing "when given a field search using the '!=' operator"
        (is-parse= [:expr [:statement
                     [:field-search
                      [:field "power"]
                      [:operator "!="]
                      [:field-value [:field-string "max"]]]]]
                   "power!=max")))

    (testing "should produce parse trees with the complement expr operator"
      (testing "when the complement operator ('!') is immediately before a field name"
        (is-parse= [:expr [:expr-complement
                      [:statement
                       [:field-search
                        [:field "power"]
                        [:operator ":"]
                        [:field-value [:field-string "max"]]]]]]
                   "!power:max"))

      (testing "when the complement operator ('!') is outside of an expression wrapped in parens"
        (is-parse= [:expr [:expr-complement
                     [:statement
                      [:field-search
                       [:field "power"]
                       [:operator ":"]
                       [:field-value [:field-string "max"]]]]]]
                   "!(power:max)")))

    (testing "should produce parse trees with the wildcard operator"
      (testing "when the wildcard is used as a field value"
        (is-parse= [:expr [:statement
                      [:field-search
                       [:field "middlename"]
                       [:operator ":"]
                       [:field-value [:wildcard]]]]]
                   "middlename:*"))

      (testing "when the wildcard prefixes a field string search"
        (is-parse= [:expr [:statement
                     [:field-search
                      [:field "species"]
                      [:operator ":"]
                      [:field-value (dblquote :field-string [:wildcard] "bear")]]]]
                   "species:\"*bear\""))

      (testing "when the wildcard is used in a field string search"
        (is-parse= [:expr [:statement
                      [:field-search
                       [:field "species"]
                       [:operator ":"]
                       [:field-value [:field-string [:dblquote] [:wildcard] [:dblquote]]]]]]
                   "species:\"*\""))

      (testing "when the wildcard suffixes a field string search"
        (is-parse= [:expr [:statement
                     [:field-search
                      [:field "species"]
                      [:operator ":"]
                      [:field-value (dblquote :field-string "bear"
                                              [:wildcard])]]]]
                   "species:\"bear*\""))

      (testing "when two wildcard operators wrap a string search"
        (is-parse= [:expr [:statement
                      [:field-search
                       [:field "species"]
                       [:operator ":"]
                       [:field-value (dblquote :field-string [:wildcard] "bear"
                                               [:wildcard])]]]]
                   "species:\"*bear*\"")))

    (testing "should produce the parse tree in the absence of a field value"
      (testing "when the field value is a literal empty string"
        (is-parse= [:expr [:statement
                     [:field-search
                      [:field "species"]
                      [:operator ":"]
                      [:field-value [:field-string [:dblquote] "" [:dblquote]]]]]]
                   "species:\"\""))

      (testing "when the field value is omitted"
        (is-parse= [:expr [:statement
                      [:field-search
                       [:field "species"]
                       [:operator ":"]
                       [:field-value [:field-string ""]]]]]
                   "species:"))

      (testing "when the field value is omitted and used as part of a conjunction"
        (is-parse= [:expr [:conjunction
                     [:statement
                      [:field-search
                       [:field "species"]
                       [:operator ":"]
                       [:field-value [:field-string ""]]]]
                     [:statement
                      [:field-search
                       [:field "processed"]
                       [:operator ":"]
                       [:field-value [:field-string "false"]]]]]]
                   "species: processed:false"))

      (testing "when the field value is omitted and used as part of a disjunction"
        (is-parse= [:expr [:disjunction
                      [:statement
                       [:field-search
                        [:field "species"]
                        [:operator ":"]
                        [:field-value [:field-string ""]]]]
                      [:statement
                       [:field-search
                        [:field "processed"]
                        [:operator ":"]
                        [:field-value [:field-string "false"]]]]]]
                   "species: OR processed:false")))

    (testing "should produce parse trees with symbols in a quoted field search value"
      (testing "when parens are used in an unquoted field search"
        (is-parse= [:expr [:statement
                     [:field-search
                      [:field "field"]
                      [:operator ":"]
                      [:field-value (dblquote :field-string "someth(ing")]]]]
                   "field:\"someth(ing\""))

      (testing "when colon eq operator is used in an unquoted field search"
        (is-parse= [:expr [:statement
                     [:field-search
                      [:field "field"]
                      [:operator ":"]
                      [:field-value (dblquote :field-string "someth:ing")]]]]
                   "field:\"someth:ing\""))

      (testing "when lt operator is used in an unquoted field search"
        (is-parse= [:expr [:statement
                     [:field-search
                      [:field "field"]
                      [:operator ":"]
                      [:field-value (dblquote :field-string "someth<ing")]]]]
                   "field:\"someth<ing\""))

      (testing "when gt operator is used in an unquoted field search"
        (is-parse= [:expr [:statement
                     [:field-search
                      [:field "field"]
                      [:operator ":"]
                      [:field-value (dblquote :field-string "someth>ing")]]]]
                   "field:\"someth>ing\""))

      (testing "when '=' eq operator is used in an unquoted field search"
        (is-parse= [:expr [:statement
                     [:field-search
                      [:field "field"]
                      [:operator ":"]
                      [:field-value (dblquote :field-string "someth=ing")]]]]
                   "field:\"someth=ing\"")))

    (testing "should apply higher precedence for conjunction than disjunction"
      (testing "when given a query with conjunction followed by a disjunction in an expression"
        (is-parse= [:expr [:disjunction
                     [:conjunction
                      [:statement
                       [:field-search
                        [:field "firstname"]
                        [:operator ":"]
                        [:field-value [:field-string "john"]]]]
                      [:statement
                       [:field-search
                        [:field "surname"]
                        [:operator ":"]
                        [:field-value [:field-string "smith"]]]]]
                     [:statement
                      [:field-search
                       [:field "firstname"]
                       [:operator ":"]
                       [:field-value [:field-string "jane"]]]]]]
                   "(firstname:john surname:smith OR firstname:jane)"))

      (testing "when given a query with disjunction followed by conjunction in an expression"
        (is-parse= [:expr [:disjunction
                      [:statement
                       [:field-search
                        [:field "firstname"]
                        [:operator ":"]
                        [:field-value [:field-string "john"]]]]
                      [:conjunction
                       [:statement
                        [:field-search
                         [:field "firstname"]
                         [:operator ":"]
                         [:field-value [:field-string "jane"]]]]
                       [:statement
                        [:field-search
                         [:field "surname"]
                         [:operator ":"]
                         [:field-value [:field-string "smith"]]]]]]]
                   "(firstname:john OR firstname:jane surname:smith)")))

    (testing "should produce the parse tree for sophisticated searches, such as"
      (testing "when given a disjunction of conjunctions of field values"
        (is-parse= [:expr [:disjunction
                     [:conjunction
                      [:statement
                       [:field-search
                        [:field "firstname"]
                        [:operator ":"]
                        [:field-value [:field-string "jane"]]]]
                      [:statement
                       [:field-search
                        [:field "surname"]
                        [:operator ":"]
                        [:field-value [:field-string "smith"]]]]]
                     [:conjunction
                      [:statement
                       [:field-search
                        [:field "firstname"]
                        [:operator ":"]
                        [:field-value [:field-string "john"]]]]
                      [:statement
                       [:field-search
                        [:field "surname"]
                        [:operator ":"]
                        [:field-value [:field-string "smith"]]]]]]]
                   "(firstname:jane surname:smith) OR (firstname:john surname:smith)"))

      (testing "when given a disjunction of field values and one side has a complement operator"
        (is-parse= [:expr [:disjunction
                     [:conjunction
                      [:statement
                       [:field-search
                        [:field "firstname"]
                        [:operator ":"]
                        [:field-value [:field-string "jane"]]]]
                      [:statement
                       [:field-search
                        [:field "surname"]
                        [:operator ":"]
                        [:field-value [:field-string "smith"]]]]]
                     [:expr-complement
                      [:conjunction
                       [:statement
                        [:field-search
                         [:field "firstname"]
                         [:operator ":"]
                         [:field-value [:field-string "john"]]]]
                       [:statement
                        [:field-search
                         [:field "surname"]
                         [:operator ":"]
                         [:field-value [:field-string "smith"]]]]]]]]
                   "(firstname:jane surname:smith) OR !(firstname:john surname:smith)")))))
