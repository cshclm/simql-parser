(ns bitpattern.simql.parser.impl.transformer-test
  (:require [bitpattern.simql.parser.impl.transformer :as sut]
            #?(:clj [clojure.test :refer [deftest testing is are]]
               :cljs [cljs.test :refer [deftest testing is are] :include-macros true])))

(defn- field-search
  [field op value]
  [:expr [:statement
          [:field-search
           [:field field]
           [:operator op]
           [:field-value value]]]])

(deftest test-transform
  (testing "transform"
    (testing "should return the provided tree for string searches"
      (let [t [:expr [:statement [:string-search "aardvark"]]]]
        (is (= [:expr [:statement [:string-search [:string "aardvark"]]]]
               (sut/transform {} t)))))

    (testing "field-search transform"
      (let [fields {"firstname" {:datatype :string}
                    "married" {:datatype :boolean}}]
        (testing "should transform valid field searches"
          (let [t (field-search "firstname" ":" [:field-string "jane"])]
            (is (= (field-search "firstname" ":" [:string "jane"])
                   (sut/transform fields t)))))

        (testing "should transform wildcard searches as expected"
          (let [t (field-search "firstname" ":" [:wildcard])]
            (is (= (field-search "firstname" ":" [:string [:wildcard]])
                   (sut/transform fields t)))))

        (testing "should preserve all data for field searches"
          (let [t (field-search "firstname" ":"
                              [:field-string [:wildcard] "jane" [:wildcard]])]
            (is (= (field-search "firstname" ":"
                               [:string [:wildcard] "jane" [:wildcard]])
                   (sut/transform fields t)))))

        (testing "should transform field values according to datatype"
          (let [t (field-search "married" ":" [:field-string "true"])]
            (is (= (field-search "married" ":" [:boolean true])
                   (sut/transform fields t)))))

        (testing "should throw if invalid data found for a field search"
          (let [t (field-search "married" ":" [:field-string "not a boolean"])]
            (is (thrown? #?(:clj Exception :cljs js/Error)
                         (sut/transform fields t)))))

        (testing "should throw if the operator is not available for that datatype"
          (let [t (field-search "married" ">=" [:field-string "true"])]
            (is (thrown? #?(:clj Exception :cljs js/Error)
                         (sut/transform fields t)))))

        (testing "should throw if the field is not known"
          (let [t (field-search "fav-color" ":" [:field-string "blue"])]
            (is (thrown? #?(:clj Exception :cljs js/Error)
                         (sut/transform fields t)))))

        (testing "should throw if the datatype is not known"
          (let [t (field-search "fav-color" ":" [:field-string "blue"])
                fields {"favorite-color" {:datatype :color}}]
            (is (thrown? #?(:clj Exception :cljs js/Error)
                         (sut/transform fields t)))))

        (testing "should preserve metadata"
          (let [fs (with-meta
                     [:field-string [:wildcard] "jane" [:wildcard]]
                     {:level "field-string"})
                fv (with-meta [:field-value fs]
                     {:level "field-value"})
                t [:expr [:statement
                          [:field-search
                           [:field "firstname"]
                           [:operator ":"]
                           fv]]]
                result (sut/transform fields t)]
            (are [path value] (= value (meta (get-in result path)))
              [1 1 3] {:level "field-value"}
              [1 1 3 1] {:level "field-string"})))))))
