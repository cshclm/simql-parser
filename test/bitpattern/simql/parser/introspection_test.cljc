(ns bitpattern.simql.parser.introspection-test
  (:require [bitpattern.simql.parser.introspection :as sut]
            #?(:clj [clojure.test :refer [deftest testing is]]
               :cljs [cljs.test :refer [deftest testing is] :include-macros true])))

(defn dblquote
  [& args]
  (conj (apply vector [:dblquote] args) [:dblquote]))

(defn expr?
  [ctx]
  (= (:type ctx) :expr))

(defn conjunction?
  [ctx]
  (= (:type ctx) :conjunction))

(defn disjunction?
  [ctx]
  (= (:type ctx) :disjunction))

(deftest test-describe-point
  (testing "describe-point"
    (testing "with a text search"
      (testing "should find expr at the start point"
        (is (expr? (sut/describe-point "myquery" 0))))

      (testing "should throw if the query is invalid"
        (is (thrown? #?(:clj Exception :cljs js/Error) (sut/describe-point "(()" 0))))

      (testing "should return nil for an empty query"
        (is (expr? (sut/describe-point "" 0))))

      (testing "should find the text-string at the first character"
        (is (= {:span [0 7],
                :type :string-search,
                :value ["myquery"],
                :string-value "myquery"
                :string-to-point "m"}
               (sut/describe-point "myquery" 1))))

      (testing "should provide the full string as `describe-point` at the last character"
        (is (= "myquery"
               (-> "myquery"
                   (sut/describe-point 7)
                   :string-to-point))))

      (testing "should return nil beyond the end of the string"
        (is (expr? (sut/describe-point "myquery" 8)))))

    (testing "with a field search"
      (let [query "myfield:myvalue"]
        (testing "should find nil at the start point"
          (is (expr? (sut/describe-point query 0))))

        (testing "should find the field at the first character"
          (is (= {:span [0 7],
                  :type :field,
                  :value ["myfield"],
                  :string-value "myfield"
                  :string-to-point "m"}
                 (sut/describe-point query 1))))

        (testing "should provide the full string as describe-point at the last character of the field"
          (is (= "myfield"
                 (-> query
                     (sut/describe-point 7)
                     :string-to-point))))

        (testing "should return the operator when point is positioned at the operator"
          (is (= {:span [7 8],
                  :type :operator,
                  :value [":"],
                  :string-value ":"
                  :context {:field "myfield"},
                  :string-to-point ":"}
                 (sut/describe-point query 8))))

        (testing "should return the the field value at the first character after the operator"
          (is (= {:span [8 15],
                  :type :field-string,
                  :value ["myvalue"],
                  :string-value "myvalue"
                  :context {:field "myfield"},
                  :string-to-point "m"}
                 (sut/describe-point query 9))))

        (testing "should provide the full string as `describe-point` at the last character"
          (is (= "myvalue"
                 (-> query
                     (sut/describe-point 15)
                     :string-to-point))))))

    (testing "with a complex query"
      (let [query "surname:smith (jane OR john)"]
        (testing "should return nil at the start point"
          (is (expr? (sut/describe-point query 0))))

        (testing "should find the field at the first character"
          (is (= {:span [0 7],
                  :type :field,
                  :value ["surname"]
                  :string-value "surname",
                  :string-to-point "s"}
                 (sut/describe-point query 1))))

        (testing "should find the first value after the field operator"
          (is (= {:span [8 13],
                  :type :field-string,
                  :value ["smith"],
                  :string-value "smith"
                  :context {:field "surname"}
                  :string-to-point "sm"}
                 (sut/describe-point query 10))))

        (testing "should return nil immediately after the field value"
          (is (conjunction? (sut/describe-point query 14))))

        (testing "should return nil at the opening paren of the disjunction"
          (is (conjunction? (sut/describe-point query 14)))
          (is (= \( (get query 14))))

        (testing "should return nil at the first character of the text string of the disjunction"
          (is (conjunction? (sut/describe-point query 15))))

        (testing "should return the text search at the second character of the disjunction"
          (is (= {:span [15 19],
                  :type :string-search,
                  :value ["jane"],
                  :string-value "jane"
                  :string-to-point "j"}
                 (sut/describe-point query 16))))

        (testing "should return nil for the second character of the 'OR'"
          (is (disjunction? (sut/describe-point query 21)))
          (is (= \R (get query 21))))

        (testing "should return the text search at the second character of the second term of the disjunction"
          (is (= {:span [23 27],
                  :type :string-search,
                  :string-value "john"
                  :value ["john"],
                  :string-to-point "j"}
                 (sut/describe-point query 24))))))

    (testing "wildcards and quoted strings"
      (testing "should return expected value for a quoted string"
        (is (= {:span [0 7],
                :type :string-search,
                :value (dblquote "thing")
                :string-value "thing",
                :string-to-point "th"}
               (sut/describe-point "\"thing\"" 3))))

      (testing "should return expected value for values containing wildcards"
        (is (= {:span [0 9],
                :type :string-search,
                :value (dblquote [:wildcard] "thing" [:wildcard])
                :string-value "*thing*",
                :string-to-point "*th"}
               (sut/describe-point "\"*thing*\"" 4))))

      (testing "should also work for fields"
        (is (= {:span [8 16],
                :type :field-string,
                :value (dblquote [:wildcard] "bear" [:wildcard])
                :string-value "*bear*",
                :string-to-point "*b"
                :context {:field "species"}}
               (sut/describe-point "species:\"*bear*\"" 11))))

      (testing "should not descend into wildcard paths"
        (is (= {:span [8 16],
                :type :field-string,
                :value (dblquote [:wildcard] "bear" [:wildcard])
                :string-value "*bear*",
                :string-to-point "*"
                :context {:field "species"}}
               (sut/describe-point "species:\"*bear*\"" 10)))))))
