(ns bitpattern.simql.parser.datatypes-test
  (:require [bitpattern.simql.parser.datatypes :as sut]
            #?(:clj [clojure.test :refer [deftest testing is are]]
               :cljs [cljs.test :refer [deftest testing is are] :include-macros true])))

(deftest test-read-value
  (testing "read-value"
    (testing "string"
      (testing "returns expected result"
        (let [v [:field-string [:wildcard] "hello" [:wildcard]]]
          (is (= [:string [:wildcard] "hello" [:wildcard]]
                 (sut/read-value :string v))))))

    (testing "boolean"
      (testing "returns expected result for true"
        (let [v [:field-string "true"]]
          (is (= [:boolean true]
                 (sut/read-value :boolean v)))))

      (testing "returns expected result for false"
        (let [v [:field-string "false"]]
          (is (= [:boolean false]
                 (sut/read-value :boolean v)))))

      (testing "throws on unexpected value"
        (let [v [:field-string "error"]]
          (is (thrown? #?(:clj Exception :cljs js/Error)
                       (sut/read-value :boolean v)))))

      (testing "throws when more data found than was expected"
        (let [v [:field-string "true" "more stuff"]]
          (is (thrown? #?(:clj Exception :cljs js/Error)
                       (sut/read-value :boolean v))))))

    (testing "number"
      (testing "returns expected result for a decimal"
        (let [v [:field-string "1.3"]]
          (is (= [:number 1.3]
                 (sut/read-value :number v)))))

      (testing "returns expected result for a negative integer"
        (let [v [:field-string "-13"]]
          (is (= [:number -13]
                 (sut/read-value :number v)))))

      (testing "throws on unexpected value"
        (let [v [:field-string "error"]]
          (is (thrown? #?(:clj Exception :cljs js/Error)
                       (sut/read-value :number v))))))

    (testing "integer"
      (testing "returns expected result for a negative integer"
        (let [v [:field-string "-13"]]
          (is (= [:integer -13]
                 (sut/read-value :integer v)))))

      (testing "throws when encountering an unexpected value"
        (let [v [:field-string "1.3"]]
          (is (thrown? #?(:clj Exception :cljs js/Error)
                       (sut/read-value :integer v))))))))

(deftest test-valid-operation?
  (testing "valid-operation?"
    (testing "boolean"
      (testing "should consider the expected operations valid"
        (are [operation valid?]
            (= (sut/valid-operation? :boolean operation) valid?)
          "=" true
          "==" true
          ":" true
          "<=" false
          ">" false
          "<" false
          ">=" false
          "$$" false)))

    (testing "default"
      (testing "should consider all other operations valid"
        (are [operation]
            (sut/valid-operation? :other operation)
          "=" "==" ":" "<=" ">" "<" ">=" "$$")))))
