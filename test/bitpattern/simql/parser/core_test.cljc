(ns bitpattern.simql.parser.core-test
  (:require [bitpattern.simql.parser.core :as sut]
            #?(:clj [clojure.test :refer :all]
               :cljs [cljs.test :refer [deftest testing is]])))

(deftest test-create-parser
  (testing "create-parser"
    (testing "should create a parser"
      (let [config {:fields {"myfield" {:datatype :boolean}}}
            parser (sut/create-parser config)
            expected [:expr
                      [:statement
                       [:field-search
                        [:field "myfield"]
                        [:operator ":"]
                        [:field-value [:boolean true]]]]]]
        (is (= expected (parser "myfield:true")))))))
