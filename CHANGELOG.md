# Change Log
All notable changes to this project will be documented in this file. This change log follows the conventions of [keepachangelog.com](http://keepachangelog.com/).

## [Unreleased]
### Changed

## 0.1.0 - 2019-07-06
### Added
- Parser and query introspection

[Unreleased]: https://gitlab.com/cshclm/simql-parser/compare/HEAD...0.1.0
[0.1.0]: https://gitlab.com/cshclm/simql-parser/compare/0.1.0...9b4df884f65b74dc14640d090887c1747e28417e
